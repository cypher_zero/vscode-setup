all
rule 'MD007', :indent => 4
rule 'MD013', :line_length => 500
rule 'MD030', :ul_single => 3, :ol_single => 2, :ul_multi => 3, :ol_multi => 2
