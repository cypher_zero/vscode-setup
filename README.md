# VSCode Setup

My VSCode extensions and settings, scripted for setup on Windows systems.

## Usage

1.  [Install VSCode](https://code.visualstudio.com/docs/setup/windows)

    -   When installing, select the option to add to the PATH variable.

2.  Open a new command prompt and run [apply-config.cmd](./apply-config.cmd)

    -   NOTE: Should be run as regular user; **NOT** with admin rights.

## [apply-config.cmd](./apply-config.cmd)

This installs the extensions and applies the configuration files for VSCode.
Note that this script will backup your existing configuration file if it already exists
before replacing it with the config file in this repo ([settings.json](./config_files/settings.json))
