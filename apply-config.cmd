@ECHO OFF

WHERE code >nul 2>nul
IF %ERRORLEVEL% NEQ 0 (
    ECHO 'code' not found or not in PATH
    GOTO END
)

ECHO Installing VSCode extensions...
CMD /C code --install-extension bpruitt-goddard.mermaid-markdown-syntax-highlighting --force
CMD /C code --install-extension coolbear.systemd-unit-file --force
CMD /C code --install-extension DavidAnson.vscode-markdownlint --force
CMD /C code --install-extension EditorConfig.EditorConfig --force
CMD /C code --install-extension erd0s.terraform-autocomplete --force
CMD /C code --install-extension esbenp.prettier-vscode --force
CMD /C code --install-extension foxundermoon.shell-format --force
CMD /C code --install-extension GitLab.gitlab-workflow --force
CMD /C code --install-extension golang.go --force
CMD /C code --install-extension hashicorp.terraform --force
CMD /C code --install-extension lizebang.bash-extension-pack --force
CMD /C code --install-extension mads-hartmann.bash-ide-vscode --force
CMD /C code --install-extension ms-azuretools.vscode-docker --force
CMD /C code --install-extension ms-kubernetes-tools.vscode-kubernetes-tools --force
CMD /C code --install-extension ms-python.python --force
CMD /C code --install-extension ms-python.vscode-pylance --force
CMD /C code --install-extension ms-toolsai.jupyter --force
CMD /C code --install-extension ms-vscode.powershell --force
CMD /C code --install-extension ms-vscode.vscode-typescript-next --force
CMD /C code --install-extension ms-vsliveshare.vsliveshare --force
CMD /C code --install-extension redhat.project-initializer --force
CMD /C code --install-extension redhat.vscode-commons --force
CMD /C code --install-extension redhat.vscode-openshift-connector --force
CMD /C code --install-extension redhat.vscode-openshift-extension-pack --force
CMD /C code --install-extension redhat.vscode-rsp-ui --force
CMD /C code --install-extension redhat.vscode-server-connector --force
CMD /C code --install-extension redhat.vscode-yaml --force
CMD /C code --install-extension Remisa.shellman --force
CMD /C code --install-extension rogalmic.bash-debug --force
CMD /C code --install-extension rpinski.shebang-snippets --force
CMD /C code --install-extension shd101wyy.markdown-preview-enhanced --force
CMD /C code --install-extension timonwong.shellcheck --force
CMD /C code --install-extension wholroyd.jinja --force
CMD /C code --install-extension zbr.vscode-ansible --force

if exist %APPDATA%\Code\User\settings.json (
    ECHO Backing up existing configuration file to:
    ECHO ==> %APPDATA%\Code\User\settings.json.bak
    MOVE /-Y %APPDATA%\Code\User\settings.json %APPDATA%\Code\User\settings.json.bak
)
ECHO Copying in configuration file...
COPY config_files\settings.json %APPDATA%\Code\User\settings.json

ECHO Done.
:END
